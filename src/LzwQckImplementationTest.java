import org.junit.jupiter.api.Assertions;

import java.util.ArrayList;
import java.util.Random;

public class LzwQckImplementationTest {

    @org.junit.jupiter.api.Test
    void compressTest() {
        System.out.println();
        System.out.println("compressTest");
        System.out.println();
        String testText = "Compression_Compressed_Compress_Comp";
        ArrayList<Integer> compressed = LzwQckImplementation.compress(testText);
        String decompressed = LzwQckImplementation.decompress(compressed);
        System.out.println("Length of testText: " + testText.length());
        System.out.println("Length of compressed: " + compressed.size());
        double compressRatio = (1 - compressed.size() / (double) (testText.length()));
        double compressedBy = compressRatio * 100;
        System.out.println("Compress ratio: " + compressRatio);
        System.out.println("Compressed by: " + compressedBy + " %");
        System.out.println();
        System.out.println("testData     : ");
        System.out.println();
        System.out.println(testText);
        System.out.println();
        System.out.println("Compressed   : ");
        System.out.println();
        System.out.println(compressed);
        System.out.println();
        System.out.println("Decompressed : ");
        System.out.println();
        System.out.println(decompressed);
        ;
        Assertions.assertEquals(testText, decompressed);
    }

    @org.junit.jupiter.api.Test
    void compressTestWithUTF16_Characters() {
        System.out.println();
        System.out.println("compressTestWithUTF16_Characters");
        System.out.println();
        String testText = "Şemsi Paşa Pasajında Sesi Büzüşesiceler";
        ArrayList<Integer> compressed = LzwQckImplementation.compress(testText);
        String decompressed = LzwQckImplementation.decompress(compressed);
        System.out.println("Length of testText: " + testText.length());
        System.out.println("Length of compressed: " + compressed.size());
        double compressRatio = (1 - compressed.size() / (double) (testText.length()));
        double compressedBy = compressRatio * 100;
        System.out.println("Compress ratio: " + compressRatio);
        System.out.println("Compressed by: " + compressedBy + " %");
        System.out.println();
        System.out.println("testData     : ");
        System.out.println();
        System.out.println(testText);
        System.out.println();
        System.out.println("Compressed   : ");
        System.out.println();
        System.out.println(compressed);
        System.out.println();
        System.out.println("Decompressed : ");
        System.out.println();
        System.out.println(decompressed);
        ;
        Assertions.assertEquals(testText, decompressed);
    }

    @org.junit.jupiter.api.Test
    void compressTestWithUTF16_Characters2() {
        System.out.println();
        System.out.println("compressTestWithUTF16_Characters2");
        System.out.println();
        String testText = "O dönemde gayet yaygın bir isim olan adının anlamı \"en iyi amaç, gaye\" olan Aristoteles'in hayatıyla ilgili bilgiler oldukça sınırlı ve Antik Çağ'dan günümüze kalan belgeler de oldukça spekülatif olan Aristo'nun MÖ 384 veya 385'te, günümüzde Athos tepesi olarak adlandırılan tepenin yakınlarında ufak bir Makedonya kenti olan Stageira'da, Makedonya kralı II. Amyntas'ın (Philippos'un babası) hekimi olan Nikomakhos'un oğlu olarak dünyaya geldiği düşünülmektedir. MÖ 367 veya 366 'da 17-18 yaşında Platon'un Atina'daki akademisine girmesiyle Platon'un en parlak öğrencilerinden biri olan Arito, Platon'un okulundayken okuma tutkusuyla tanındığı ve \"okuyucu\" lakabını edindiği söylenir. Helenistik dönemden önce felsefe daha çok karşılıklı konuşma ve tartışma biçiminde yapıldığı için Aristo'nun metinlere yönelmesi farklı bir etkinlik olarak görülmüş olabileceği gibi, bu lakap daha sonraki Aristo okurları tarafından Aristo'nun metinlerinin kendinden önceki filozoflara göndermelerle dolu olması nedeniyle verilmiş de olabilir.[7] Bu dönemde hiçbiri günümüze bütünüyle kalmamış olan diyaloglarını yazmaya başladığı düşünülmüktedir.\n" +
                "\n" +
                "Platon MÖ 347'de öldüğünde, Akademi'nin başına yeğeni Spevsippos geçmiştir, Aristo'nun Atina'dan ayrılmasına genellikle bu durum temel neden olarak gösterilse de o dönemde Makedonya'nın güçlenmesi ve diğer Yunan şehir devletlerini tehdit etmesi sonucu gelişen Makedon düşmanlığının da Atina'dan ayrılmasında etkili olduğu düşünülebilir.[8] Ksenokratos'la beraber bulunduğu Assos kentinin tiranı Atarnevus'lu Hermias'ın yanına danışman olarak gider, bu sırada en önemli öğrencilerinden biri olan Theophrastus'la beraber özellikle Midilli adasında hayvanlar, bitkiler ve coğrafya hakkında pek çok gözlem, inceleme ve deney yaptığı, bu konulardaki metinlerini dolduran örneklerin çoğunu bu dönemde topladığı düşünülmektedir. Midilli'deyken Hermias'ın yeğeni ya da evlatlık kızı Püthias'la evlenir ve yine Püthias adında bir kızı olur.\n" +
                "\n" +
                "343'te Pella'daki (Bugün Ayii Apostili) Makedonya Kralı II. Filip'in sarayına danışman olarak gider, burada Filip'in oğlu İskender ve daha sonra ordusunda general, ve İskender öldükten sonra sırasıyla biri Yunan Yarımadasında diğeri Mısır'da kral olacak olan Cassender ve I. Ptolemaios'a hocalık yapar.[9] Antik Çağ filozoflarının hayatlarıyla ilgili çoğu rivayete dayanan bilgilerden oluşan bir biyografi kitabı yazmış olan Diogenes Laertius, 341 yılında Perslerin eline düşen Hermias'ın öldürülünce Aristo'nun Delfi'de ona bir anıt yaptırdığını ve bu anıta onun anısına bir şiir yazdığını aktarır ve şiire de yer verir.[10] Aristo'nun Perslere karşı etnik ayrımcılık yaptığı, Yunanları Perslerden daha üstün gördüğü, öğrencilerine Yunanlara karşı iyi davranan fakat aynı düzeyde Perslere kötü davranan lider olmaları gerektiğini öğrettiği söylense de,[9] ancak zaten Pers düşmanlığı ve \"dostuna iyi, düşmanına kötü davran\" normu Antik Yunan toplumunda Aristo'ya kadar en az iki yüzyıldır yaygın biçimde görülmektedir, ayrıca Aristo felsefesiyle öğrencilerinin liderlikleri arasında net bir örüntü görülmemekte, Aristo'nun bu dönemde seçkin öğrencilerine ne öğrettiği bilinmemektedir.\n" +
                "\n" +
                "Filip'in ölümüyle MÖ 335 yılında İskender Makedonya Kralı olduğunda Aristoteles Atina'ya dönüp daha öncesinde de felsefe amacıyla kullanılmış bir yer olan Atina kent merkezine yakın Lükeion'da kendine ait bir felsefe okulu kurar ve takipçilerine ya (rivayete göre) Aristo öğrencileriyle dolaşarak tartıştığı için ya da bir tür çevresi sütunlarla çevrili avlu ya da galeri olan mimarinin adından dolayısıyla Peripatetikler denmiştir (bu isim hem \"etrafında yürüyenler\" hem de \"bir alanı çevreleyen mimari yapı\" anlamına gelebilir).[11][12] Burada on iki sene ders veren Aristo eşi Püthias ölünce Herpüllis'le evlenir ve Nikomakhos adında bir oğlu olur. Günümüze kalan metinlerinin çok büyük ihtimalle bu dönemde Aristo ya da öğrencileri tarafından yaptıkları tartışmalara dair notlar olduğu ve okul dışında paylaşılmadığı düşünülmektedir.\n" +
                "\n" +
                "MÖ 323'te Büyük İskender'in ölmesi sonrası Atina'da Makedon karşıtı bir tepki dalgası oluşunca Makedon olan Aristoteles'e karşı, dine saygısızlık davası açılması söz konusu olur. Bir ölümlüyü -Hermias'ı- anısına bir ilâhi yazarak ölümsüzleştirmekle itham edilir.[9] Bunun üzerine Aristoteles, Sokrates'in yazgısını paylaşmak yerine Atina'yı terk etmeyi seçer: kendi deyişiyle, Atinalılar'a \"felsefeye karşı ikinci bir suç işlemeleri\" fırsatını tanımak istemez.[13] Annesinin memleketi olan Eğriboz (Evboia) adasındaki Helke'ye sığınır. Ertesi yıl MÖ 322'de, altmış iki yaşında hayatını kaybeder.";
        for (int i = 0; i < 3; i++)
        {
            testText += testText;
        }
        ArrayList<Integer> compressed = LzwQckImplementation.compress(testText);
        String decompressed = LzwQckImplementation.decompress(compressed);
        System.out.println("Length of testText: " + testText.length());
        System.out.println("Length of compressed: " + compressed.size());
        double compressRatio = (1 - compressed.size() / (double) (testText.length()));
        double compressedBy = compressRatio * 100;
        System.out.println("Compress ratio: " + compressRatio);
        System.out.println("Compressed by: " + compressedBy + " %");
        System.out.println();
        System.out.println("testData     : ");
        System.out.println();
        System.out.println(testText);
        System.out.println();
        System.out.println("Compressed   : ");
        System.out.println();
        System.out.println(compressed);
        System.out.println();
        System.out.println("Decompressed : ");
        System.out.println();
        System.out.println(decompressed);
        ;
        Assertions.assertEquals(testText, decompressed);
    }
}
