import java.lang.reflect.Array;
import java.nio.Buffer;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Hashtable;

public class LzwQckImplementation {

    public static ArrayList<Integer> compress(String data)
    {
        Hashtable<String, Integer> table = createDictionary();
        ArrayList<Integer> output = new ArrayList<Integer>();
        String s = "";
        char ch;
        String temp;
        int buffer;
        for(int i = 0; i < data.length(); i++)
        {
            ch = data.charAt(i);
            temp = s + ch;
            if(table.containsKey(temp))
            {
                s += ch;
            }
            else
            {
                buffer = table.get(s);
                output.add(buffer);
                table.put(temp, table.size());
                s = Character.toString(ch);
            }
        }
        buffer = table.get(s);
        output.add(buffer);
        return output;
    }

    public static String getCharAt(String str, int index)
    {
        return Character.toString(str.charAt(index));
    }

    public static byte[] toBytes(int i)
    {
        byte[] result = new byte[4];

        result[0] = (byte) (i >> 24);
        result[1] = (byte) (i >> 16);
        result[2] = (byte) (i >> 8);
        result[3] = (byte) (i /*>> 0*/);

        return result;
    }

    public static String decompress(ArrayList<Integer> data)
    {
        Hashtable<Integer, String> table = createInverseDictionary();
        int k = data.get(0);;
        String s = table.get(k);
        data.remove(0);
        StringBuilder builder = new StringBuilder(s);
        String temp = null;
        for (int i = 0; i < data.size(); i++)
        {
            k = data.get(i);
            if(table.containsKey(k))
            {
                temp = table.get(k);
            }
            else if(k == table.size())
            {
                temp = s + s.charAt(0);
            }
            builder.append(temp);
            table.put(table.size(),s + temp.charAt(0));
            s = temp;
        }
        return builder.toString();
    }

    public static Hashtable<String, Integer> createDictionary()
    {
        Hashtable<String, Integer> table = new Hashtable<String, Integer>();
        for (int i = 0; i < Character.MAX_VALUE / 2; i++)
        {
            table.put(Character.toString((char)i),i);
        }
        return table;
    }

    public static Hashtable<Integer, String> createInverseDictionary()
    {
        Hashtable<Integer, String> table = new Hashtable<Integer, String>();
        for (int i = 0; i < Character.MAX_VALUE / 2; i++)
        {
            table.put(i,Character.toString((char)i));
        }
        return table;
    }
}
